const canvas = document.getElementById("screen")
const ctx = canvas.getContext("2d")

// détermine la taille de mon écran
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

var cRect = canvas.getBoundingClientRect()

var screenWidth = innerWidth - cRect.left
var screenHeight = innerHeight - cRect.top

//Pour avoir une couleur aléatoire
var color = '#' + Math.floor(Math.random() * 16777215).toString(16);
var position = [
    [300, 300],
    [400, 400],
    [500, 400],
    [400, 300],
    [300, 300],
]
var directions = [
    [true, true],
    [true, false],
    [false, true],
    [false, false],
]

canvas.width = screenWidth
canvas.height = screenHeight

function Point(){
    this.x = parseInt(Math.random()*canvas.width);
    this.y = parseInt(Math.random()*canvas.height);
}
function newCanvas() {
    ctx.fillStyle = 'gba(0,0,0,0.1)';
    ctx.fillRect(0, 0, canvas.width, canvas.height);
drawShape()
}

    
    function drawShape() {
        ctx.strokeStyle = color;
        let a = 0
        for (let j = 0; j < 13; j++) {
            ctx.beginPath()

            ctx.moveTo(position[0][0] + a, position[0][1] + a)
            for (let i = 1; i <= position.length-1; i++) {
                ctx.lineTo(position[i][0] + a, position[i][1] + a)
            }
            ctx.stroke()
            a += 5

        }
        setTimeout(moveShape, 100)

    }


    function moveShape() {
        ctx.strokeStyle = color;
        for (let i = 0; i < position.length - 1; i++) {
            moveBorder(i)
        }
        newCanvas()
    }

    function moveBorder(edgeNumber){

        if (directions[edgeNumber][0] === true && directions[edgeNumber][1] === true) {
            position[edgeNumber][0] += 20
            position[edgeNumber][1] += 20
            if (edgeNumber === 0) {
                position[4][0] += 20
                position[4][1] += 20
            }
        }
        if (directions[edgeNumber][0] === true && directions[edgeNumber][1] !== true) {
            position[edgeNumber][0] += 20
            position[edgeNumber][1] -= 20
            if (edgeNumber === 0) {
                position[4][0] += 20
                position[4][1] -= 20
            }
        }
        if (directions[edgeNumber][0] !== true && directions[edgeNumber][1] === true) {
            position[edgeNumber][0] -= 20
            position[edgeNumber][1] += 20
            if (edgeNumber === 0) {
                position[4][0] -= 20
                position[4][1] += 20
            }
        }
        if (directions[edgeNumber][0] !== true && directions[edgeNumber][1] !== true) {
            position[edgeNumber][0] -= 20
            position[edgeNumber][1] -= 20
            if (edgeNumber === 0) {
                position[4][0] -= 20
                position[4][1] -= 20
            }
        }
        changeDirections(edgeNumber)

    }
    function changeDirections(edgeNumber) {
        if (position[edgeNumber][0] >= screenWidth) {
            directions[edgeNumber][0] = false
        }
        else if (position[edgeNumber][0] <= 0) {
            directions[edgeNumber][0] = true
        }
        if (position[edgeNumber][1] >= screenHeight) {
            directions[edgeNumber][1] = false
        } else if (position[edgeNumber][1] <= 0) {
            directions[edgeNumber][1] = true
        }


}


        drawShape()

